Summary:   A popular and easy to use graphical IRC (chat) client
Name:      hexchat
Version:   2.12.3
Release:   1.gita0e80eb%{?dist}
Group:     Applications/Internet
License:   GPLv2+
URL:       https://hexchat.github.io
Source:    https://dl.hexchat.net/hexchat/%{name}-%{version}.tar.xz

# RHEL or derivative.
%if 0%{?rhel} >= 7
BuildRequires: python-devel
# Fedora and other distros.
%else
BuildRequires: python3-devel
%endif
BuildRequires: perl-ExtUtils-Embed
BuildRequires: pciutils-devel
# RHEL or derivative.
%if 0%{?rhel} >= 7
BuildRequires: lua-devel
# Fedora and other distros.
%else
#%ifarch %{arm} %{ix86} x86_64
#BuildRequires: luajit-devel
#%else
BuildRequires: lua-devel
#%endif
%endif
BuildRequires: dbus-glib-devel
BuildRequires: intltool
BuildRequires: libtool
BuildRequires: glib2-devel
BuildRequires: gtk2-devel
BuildRequires: libcanberra-devel
BuildRequires: libproxy-devel
BuildRequires: libnotify-devel
BuildRequires: openssl-devel
BuildRequires: desktop-file-utils
BuildRequires: hicolor-icon-theme
BuildRequires: sound-theme-freedesktop
BuildRequires: iso-codes-devel

%description
HexChat is an easy to use graphical IRC chat client for the X Window System.
It allows you to join multiple IRC channels (chat rooms) at the same time,
talk publicly, private one-on-one conversations etc. Even file transfers
are possible.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name}%{?_isa} = %{version}-%{release}
%description devel
This package contains the development files for %{name}.

%prep
%autosetup

%build
# RHEL or derivative.
%if 0%{?rhel} >= 7
%configure
# Fedora and other distros.
%else
%configure --enable-python=python3
%endif

%make_build

%install
%make_install

# Get rid of libtool archives
find %{buildroot} -name '*.la' -exec rm -f {} ';'

%find_lang %{name}

%post
/bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null || :
/usr/bin/update-desktop-database &> /dev/null || :

%postun
/usr/bin/update-desktop-database &> /dev/null || :
if [ $1 -eq 0 ] ; then
    /bin/touch --no-create %{_datadir}/icons/hicolor &>/dev/null
    /usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :
fi

%posttrans
/usr/bin/gtk-update-icon-cache %{_datadir}/icons/hicolor &>/dev/null || :

%files -f %{name}.lang
%{_bindir}/hexchat
%license COPYING
%doc readme.md
%dir %{_libdir}/hexchat
%dir %{_libdir}/hexchat/plugins
%{_libdir}/hexchat/plugins/checksum.so
%{_libdir}/hexchat/plugins/doat.so
%{_libdir}/hexchat/plugins/fishlim.so
%{_libdir}/hexchat/plugins/lua.so
%{_libdir}/hexchat/plugins/sysinfo.so
%{_libdir}/hexchat/plugins/perl.so
%{_libdir}/hexchat/plugins/python.so
%{_datadir}/applications/hexchat.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/appdata/hexchat.appdata.xml
%{_datadir}/dbus-1/services/org.hexchat.service.service
%{_mandir}/man1/*.gz

%files devel
%{_includedir}/hexchat-plugin.h
%{_libdir}/pkgconfig/hexchat-plugin.pc

%changelog
* Sat Oct 22 2016 Patrick Griffis <tingping@tingping.se> - 2.12.3-1
- Version bump to 2.12.3
- Fix building against OpenSSL 1.1.0

* Sat Oct 8 2016 Patrick Griffis <tingping@tingping.se> - 2.12.2-1
- Version bump to 2.12.2

* Tue Jul 5 2016 TingPing <tingping@tingping.se> - 2.12.1-3
- Fix input style theming with Adwaita

* Tue May  3 2016 Peter Robinson <pbrobinson@fedoraproject.org> 2.12.1-2
- Not all arches have luajit

* Sun May 1 2016 TingPing <tingping@tingping.se> - 2.12.1-1
- Version bump to 2.12.1

* Sat Mar 12 2016 TingPing <tingping@tingping.se> - 2.12.0-1
- Version bump to 2.12.0

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 2.10.2-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Thu Nov 12 2015 Kevin Fenzi <kevin@scrye.com> - 2.10.2-6
- Build against python 3.5 specifically.

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10.2-5
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Wed Jun 17 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10.2-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Sat Jun 06 2015 Jitka Plesnikova <jplesnik@redhat.com> - 2.10.2-3
- Perl 5.22 rebuild

* Fri Jan 30 2015 TingPing <tingping@tingping.se> - 2.10.2-2
- Do not own icon directories owned by hicolor-icon-theme (#1171904)
- Build against python3
- Make use of license macro

* Tue Nov 25 2014 TingPing <tingping@tingping.se> - 2.10.2-1
- Version bump to 2.10.2

* Thu Aug 28 2014 Jitka Plesnikova <jplesnik@redhat.com> - 2.10.1-3
- Perl 5.20 rebuild

* Sat Aug 16 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_22_Mass_Rebuild

* Mon Jul 28 2014 TingPing <tingping@tingping.se> - 2.10.1-1
- Version bump to 2.10.1

* Sat Jun 07 2014 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.10.0-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_21_Mass_Rebuild

* Mon Jun 2 2014 TingPing <tingping@tingping.se> - 2.10.0-1
- Version bump to 2.10.0

* Mon Sep 16 2013 TingPing <tingping@tingping.se> - 2.9.6.1-1
- Version bump to 2.9.6.1

* Wed Sep 11 2013 TingPing <tingping@tingping.se> - 2.9.6-1
- Version bump to 2.9.6

* Sat Aug 03 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.9.5-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_20_Mass_Rebuild

* Sat Aug 03 2013 Kevin Fenzi <kevin@scrye.com> 2.9.5-2
- Rebuild for new perl

* Mon Apr 1 2013 TingPing <tingping@tingping.se> - 2.9.5-1
- Version bump to 2.9.5

* Thu Feb 14 2013 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.9.4-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_19_Mass_Rebuild

* Thu Dec 27 2012 TingPing <tingping@tingping.se> - 2.9.4-1
- Initial HexChat package
